## Description

This module uses the Symphony core IBAN validator and
implements a webform IBAN-field.

## Dependencies

- [Webform](https://www.drupal.org/project/webform)

## Usage
- Install with composer
- Create a webform
- Add an element, search for IBAN

## Credits
[Howard Ge](https://www.drupal.org/user/174740) for the original version.
